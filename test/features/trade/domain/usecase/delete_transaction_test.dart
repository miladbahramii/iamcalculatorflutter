import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:iAmCalculatorFlutter/core/entities/state_act_status.dart';
import 'package:iAmCalculatorFlutter/features/transaction/domain/entities/transaction.dart';
import 'package:iAmCalculatorFlutter/features/transaction/domain/repositories/transaction_repository.dart';
import 'package:iAmCalculatorFlutter/features/transaction/domain/usecases/delete_transaction.dart';
import 'package:mockito/mockito.dart';

class MockTransactionRepository extends Mock implements TransactionRepository {}

main() async {
  MockTransactionRepository mockTransactionRepository;
  DeleteTransaction usecase;

  setUp(() async {
    mockTransactionRepository = MockTransactionRepository();
    usecase = DeleteTransaction(mockTransactionRepository);
  });

  final tId = 1;
  final tMessage =
      StateActStatus(completed: true, message: 'transaction deleted');

  test('should add transaction', () async {
    when(mockTransactionRepository.deleteTransaction(tId))
        .thenAnswer((_) async => Right(tMessage));
    final result = await usecase(tId);

    expect(result, Right(tMessage));
    verify(mockTransactionRepository.deleteTransaction(tId));
    verifyNoMoreInteractions(mockTransactionRepository);
  });
}
