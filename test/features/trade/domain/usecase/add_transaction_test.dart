import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:iAmCalculatorFlutter/core/entities/state_act_status.dart';
import 'package:iAmCalculatorFlutter/features/transaction/domain/entities/transaction.dart';
import 'package:iAmCalculatorFlutter/features/transaction/domain/repositories/transaction_repository.dart';
import 'package:iAmCalculatorFlutter/features/transaction/domain/usecases/add_transaction.dart';
import 'package:mockito/mockito.dart';

class MockTransactionRepository extends Mock implements TransactionRepository {}

main() async {
  MockTransactionRepository mockTransactionRepository;
  AddTransaction usecase;

  setUp(() async {
    mockTransactionRepository = MockTransactionRepository();
    usecase = AddTransaction(mockTransactionRepository);
  });

  final tTransaction =
      Transaction(amount: 50, id: 1, isDebtor: 1, name: 'test', userId: 1);
  final tMessage =
      StateActStatus(completed: true, message: 'transaction added');

  test('should add transaction', () async {
    when(mockTransactionRepository.addTransaction(tTransaction))
        .thenAnswer((_) async => Right(tMessage));
    final result = await usecase(tTransaction);

    expect(result, Right(tMessage));
    verify(mockTransactionRepository.addTransaction(tTransaction));
    verifyNoMoreInteractions(mockTransactionRepository);
  });
}
