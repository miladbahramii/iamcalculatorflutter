import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:iAmCalculatorFlutter/features/transaction/domain/entities/transaction.dart';
import 'package:iAmCalculatorFlutter/features/transaction/domain/repositories/transaction_repository.dart';
import 'package:iAmCalculatorFlutter/features/transaction/domain/usecases/get_transactions.dart';
import 'package:mockito/mockito.dart';

class MockTransactionRepository extends Mock implements TransactionRepository {}

main() async {
  MockTransactionRepository mockTransactionRepository;
  GetTransactions usecase;

  setUp(() async {
    mockTransactionRepository = MockTransactionRepository();
    usecase = GetTransactions(mockTransactionRepository);
  });

  final List<Transaction> transactions = [
    Transaction(amount: 50, id: 1, isDebtor: 1, name: 'test', userId: 1),
    Transaction(amount: 50, id: 2, isDebtor: 1, name: 'test', userId: 1),
  ];
  final tUserId = 1;

  test('should get transactions', () async {
    when(mockTransactionRepository.getTransactions(tUserId))
        .thenAnswer((_) async => Right(transactions));
    final result = await usecase(tUserId);

    expect(result, Right(transactions));
    verify(mockTransactionRepository.getTransactions(tUserId));
    verifyNoMoreInteractions(mockTransactionRepository);
  });
}
