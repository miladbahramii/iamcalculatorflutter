import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:iAmCalculatorFlutter/core/usecases/usecase.dart';
import 'package:iAmCalculatorFlutter/features/user/domain/entities/user.dart';
import 'package:iAmCalculatorFlutter/features/user/domain/repositories/user_repository.dart';
import 'package:iAmCalculatorFlutter/features/user/domain/usecases/get_users.dart';
import 'package:mockito/mockito.dart';

class MockUserRepository extends Mock implements UserRepository {}

main() {
  MockUserRepository mockUserRepository;
  GetUsers usecase;

  setUp(() async {
    mockUserRepository = MockUserRepository();
    usecase = GetUsers(mockUserRepository);
  });

  final users = [
    User(
        id: 1,
        name: 'milad',
        phone: 09125585804,
        address: 'pardis',
        remnant: 120000),
    User(
        id: 1,
        name: 'milad',
        phone: 09125585804,
        address: 'pardis',
        remnant: 120000),
  ];

  test('should get users repository.', () async {
    when(mockUserRepository.getUsers()).thenAnswer((_) async => Right(users));

    final result = await usecase(NoParams());
    expect(result, Right(users));
  });
}
