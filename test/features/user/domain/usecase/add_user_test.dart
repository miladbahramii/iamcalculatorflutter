import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:iAmCalculatorFlutter/core/entities/state_act_status.dart';
import 'package:iAmCalculatorFlutter/features/user/domain/entities/user.dart';
import 'package:iAmCalculatorFlutter/features/user/domain/repositories/user_repository.dart';
import 'package:iAmCalculatorFlutter/features/user/domain/usecases/add_user.dart';
import 'package:mockito/mockito.dart';

class MockUserRepository extends Mock implements UserRepository {}

main() {
  MockUserRepository mockUserRepository;
  AddUser usecase;

  setUp(() async {
    mockUserRepository = MockUserRepository();
    usecase = AddUser(mockUserRepository);
  });

  final StateActStatus tCompeletedAction =
      StateActStatus(completed: true, message: 'new user added');
  final tUser =
      User(id: 1, name: 'milad', phone: 0935, address: 'pardis', remnant: 0);

  test('should add new user.', () async {
    when(mockUserRepository.addUser(tUser.name, tUser.address, tUser.phone))
        .thenAnswer((_) async => Right(tCompeletedAction));

    final result = await usecase(AddParams(
        name: tUser.name, address: tUser.address, phone: tUser.phone));
    expect(result, Right(tCompeletedAction));
    verify(mockUserRepository.addUser(tUser.name, tUser.address, tUser.phone));
    verifyNoMoreInteractions(mockUserRepository);
  });
}
