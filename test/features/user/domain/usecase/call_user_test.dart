import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:iAmCalculatorFlutter/core/entities/state_act_status.dart';
import 'package:iAmCalculatorFlutter/features/user/domain/repositories/user_repository.dart';
import 'package:iAmCalculatorFlutter/features/user/domain/usecases/call_user.dart';
import 'package:mockito/mockito.dart';

class MockUserRepository extends Mock implements UserRepository {}

main() {
  MockUserRepository mockUserRepository;
  CallUser usecase;

  setUp(() async {
    mockUserRepository = MockUserRepository();
    usecase = CallUser(mockUserRepository);
  });

  final StateActStatus tCompeletedAction =
      StateActStatus(completed: true, message: 'called user phone');
  final num tNumber = 12;

  test('should call user and get delivery status.', () async {
    when(mockUserRepository.callUser(any))
        .thenAnswer((_) async => Right(tCompeletedAction));

    final result = await usecase(tNumber);
    expect(result, Right(tCompeletedAction));
    verify(mockUserRepository.callUser(tNumber));
    verifyNoMoreInteractions(mockUserRepository);
  });
}
