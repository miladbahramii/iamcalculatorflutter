import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:iAmCalculatorFlutter/core/entities/state_act_status.dart';
import 'package:iAmCalculatorFlutter/features/user/domain/repositories/user_repository.dart';
import 'package:iAmCalculatorFlutter/features/user/domain/usecases/delete_user.dart';
import 'package:mockito/mockito.dart';

class MockUserRepository extends Mock implements UserRepository {}

main() {
  MockUserRepository mockUserRepository;
  DeleteUser usecase;

  setUp(() async {
    mockUserRepository = MockUserRepository();
    usecase = DeleteUser(mockUserRepository);
  });

  final StateActStatus tCompeletedAction =
      StateActStatus(completed: true, message: 'user deleted');
  final tParams = Params(id: 1);

  test('should delete user from database.', () async {
    when(mockUserRepository.deleteUser(tParams.id))
        .thenAnswer((_) async => Right(tCompeletedAction));
    final result = await usecase(tParams);
    expect(result, Right(tCompeletedAction));
    verify(mockUserRepository.deleteUser(tParams.id));
    verifyNoMoreInteractions(mockUserRepository);
  });
}
