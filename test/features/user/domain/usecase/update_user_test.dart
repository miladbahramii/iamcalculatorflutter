import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:iAmCalculatorFlutter/core/entities/state_act_status.dart';
import 'package:iAmCalculatorFlutter/features/user/domain/entities/user.dart';
import 'package:iAmCalculatorFlutter/features/user/domain/repositories/user_repository.dart';
import 'package:iAmCalculatorFlutter/features/user/domain/usecases/update_user.dart';
import 'package:mockito/mockito.dart';

class MockUserRepository extends Mock implements UserRepository {}

main() {
  MockUserRepository mockUserRepository;
  UpdateUser usecase;

  setUp(() async {
    mockUserRepository = MockUserRepository();
    usecase = UpdateUser(mockUserRepository);
  });

  final StateActStatus tCompeletedAction =
      StateActStatus(completed: true, message: 'user updated');
  final tUser =
      User(id: 1, name: 'milad', phone: 0935, address: 'pardis', remnant: 0);

  test('should update user from database.', () async {
    when(mockUserRepository.updateUser(tUser))
        .thenAnswer((_) async => Right(tCompeletedAction));

    final result = await usecase(tUser);
    expect(result, Right(tCompeletedAction));
    verify(mockUserRepository.updateUser(tUser));
    verifyNoMoreInteractions(mockUserRepository);
  });
}
