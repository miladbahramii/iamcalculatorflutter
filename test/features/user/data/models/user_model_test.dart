import 'package:flutter_test/flutter_test.dart';
import 'package:iAmCalculatorFlutter/features/user/data/models/user_model.dart';
import 'package:iAmCalculatorFlutter/features/user/domain/entities/user.dart';

main() {
  final tUserModel = UserModel(
      id: 1, name: 'milad', address: 'pardis', phone: 0912, remnant: 50000);

  test('should be a subclass of User', () async {
    expect(tUserModel, isA<User>());
  });

  group('toJson', () {
    test(
      'should return a JSON map containing the proper data',
      () async {
        // act
        final result = tUserModel.toJson();
        // assert
        final expectedMap = {
          "id": 1,
          "name": 'milad',
          "address": 'pardis',
          "phone": 0912,
          "remnant": 50000
        };
        expect(result, expectedMap);
      },
    );
  });
}
