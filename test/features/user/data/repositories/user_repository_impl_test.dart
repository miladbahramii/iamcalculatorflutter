import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:iAmCalculatorFlutter/core/error/exeptions.dart';
import 'package:iAmCalculatorFlutter/core/error/failures.dart';
import 'package:iAmCalculatorFlutter/core/network/network_info.dart';
import 'package:iAmCalculatorFlutter/features/user/data/datasources/user_local_data_source.dart';
import 'package:iAmCalculatorFlutter/features/user/data/models/user_model.dart';
import 'package:iAmCalculatorFlutter/features/user/data/repositories/user_repository_impl.dart';
import 'package:mockito/mockito.dart';
import 'package:iAmCalculatorFlutter/core/error/failures.dart';

class MockLocalDataSource extends Mock implements UserLocalDataSource {}

class MockNetworkInfo extends Mock implements NetworkInfo {}

main() {
  UserRepositoryImpl repository;
  MockLocalDataSource mockLocalDataSource;
  MockNetworkInfo mockNetworkInfo;

  setUp(() {
    mockLocalDataSource = MockLocalDataSource();
    mockNetworkInfo = MockNetworkInfo();
    repository = UserRepositoryImpl(
        localDataSource: mockLocalDataSource, networkInfo: MockNetworkInfo());
  });

  group('localDataSource', () {
    final List<UserModel> tUsers = [
      UserModel(id: 1, name: 'milad', address: 'pardis', phone: 09, remnant: 0),
      UserModel(id: 1, name: 'milad', address: 'pardis', phone: 09, remnant: 0),
    ];
    test('should get users', () async {
      // arrange
      when(mockLocalDataSource.getUsers()).thenAnswer((_) async => tUsers);
      // act
      final result = await repository.getUsers();
      // assert
      verify(mockLocalDataSource.getUsers());
      expect(result, Right(tUsers));
    });

    test(
        'should return database failure when when the call to local data source unsuccessful',
        () async {
      // arrange
      when(mockLocalDataSource.getUsers()).thenThrow(DatabaseExeption());
      // act
      final result = await repository.getUsers();
      // assert
      verify(mockLocalDataSource.getUsers());
      expect(result, Left(LocalDatabaseFailure()));
    });
  });
}
