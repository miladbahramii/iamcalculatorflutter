import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:iAmCalculatorFlutter/core/utils/trade_calculator.dart';
import 'package:iAmCalculatorFlutter/features/trade/domain/entities/trade.dart';

main() {
  TradeCalculator tradeCalculator = TradeCalculator();
  
  test('should ', () async {
    // arrange
    List<Trade> tTrades = [
      Trade(
          id: 1,
          name: 'test1',
          isDebtor: 0,
          amount: 150,
          amountStr: '150',
          date: DateTime.now().toString(),
          remnant: 0,
          userId: 1),
      Trade(
          id: 1,
          name: 'test2',
          isDebtor: 0,
          amount: -50,
          amountStr: '150',
          date: DateTime.now().toString(),
          remnant: 0,
          userId: 1),
    ];
    List<Trade> matcher = [
      Trade(
          id: 1,
          name: 'test1',
          isDebtor: 0,
          amount: 150,
          amountStr: '150',
          date: DateTime.now().toString(),
          remnant: 150,
          userId: 1),
      Trade(
          id: 1,
          name: 'test2',
          isDebtor: 0,
          amount: -50,
          amountStr: '150',
          date: DateTime.now().toString(),
          remnant: 100,
          userId: 1),
    ];
    // act
    final result = tradeCalculator.calculateTrades(tTrades);
    // assert
    expect(result, Right(matcher));
  });
}
