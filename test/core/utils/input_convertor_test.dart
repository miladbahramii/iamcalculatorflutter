import 'package:flutter_test/flutter_test.dart';
import 'package:iAmCalculatorFlutter/core/utils/input_convertor.dart';

main() async {
  InputConvertor inputConvertor;

  setUp() async {
    inputConvertor = InputConvertor();
  }

  group('numberToSignedString', () {
    test('should return signed string', () async {
      // arrange
      final int tNumber = 123000000;
      final String tMatcher = '123,000,000';
      // act
      final result = inputConvertor.numberToCurrency(tNumber);
      // assert
      expect(result, tMatcher);
    });
  });
}
