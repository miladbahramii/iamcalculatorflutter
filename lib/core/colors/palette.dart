import 'package:flutter/material.dart';

abstract class MainPallet {}

class Palette extends MainPallet {
  static const primary = Colors.blue;
  static final Color secondary = Colors.green.shade200;
  static final Color dark = Colors.grey.shade700;
}
