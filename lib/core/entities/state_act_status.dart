import 'package:meta/meta.dart';

class StateActStatus {
  bool completed;
  String message;

  StateActStatus({@required this.completed, this.message});
}
