import 'package:flutter/material.dart';

class TouchableIcon extends StatelessWidget {
  final IconData icon;
  final EdgeInsetsGeometry padding;
  final Color color;
  final onTap;
  final double size;

  TouchableIcon({
    @required this.icon,
    this.padding,
    this.color,
    this.onTap,
    this.size
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: padding,
      child: InkWell(
        onTap: onTap,
        child: Icon(
          icon,
          color: color,
          size: size,
        ),
      )
    );
  }
}