import 'package:flutter/material.dart';

class CurrencyWidget extends StatelessWidget {
  final String amountStr;
  const CurrencyWidget({Key key, this.amountStr}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(amountStr, style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, fontFamily: 'IranSansNumber'),),
            SizedBox(
              width: 3,
            ),
            Text('تومان', style: TextStyle(fontWeight: FontWeight.bold)),
          ],
        ),
      ),
    );
  }
}
