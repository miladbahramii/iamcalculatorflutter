import 'package:flutter/material.dart';

class Headline extends StatelessWidget {
  final String text;
  final Color color;
  Headline({Key key, @required this.text, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(right: 10),
      child: RichText(
        overflow: TextOverflow.ellipsis,
        strutStyle: StrutStyle(fontSize: 22.0),
        text: TextSpan(
            style: TextStyle(
                color: color,
                fontFamily: 'IranSansNumber',
                fontWeight: FontWeight.bold,
                fontSize: 16),
            text: text),
      ),
    );
  }
}
