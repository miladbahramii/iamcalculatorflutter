import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

class RadioBox<Type> extends StatelessWidget {
  final Type groupValue;
  final Type value;
  final onChanged;
  final String title;
  const RadioBox(
      {Key key,
      @required this.onChanged,
      @required this.title,
      @required this.groupValue,
      @required this.value})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Radio(
            value: value,
            groupValue: groupValue,
            onChanged: onChanged,
          ),
          Text(
            title,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 16
            ),
          ),
        ],
      ),
    );
  }
}
