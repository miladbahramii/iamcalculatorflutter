import 'package:flutter/material.dart';

enum Status { creditor, deptor, clear }

class CurrencyBadge extends StatelessWidget {
  final int amount;
  const CurrencyBadge({Key key, this.amount}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50),
        color: getStatusColor(amount),
      ),
      child: Text(
        getStatusText(amount),
        style: TextStyle(
            fontSize: 10, color: Colors.white, fontFamily: 'IranSansNumber'),
      ),
    );
  }

  String getStatusText(int amount) {
    if (amount < 0) {
      return 'بدهکار';
    } else if (amount > 0) {
      return 'بستانکار';
    } else if (amount == 0) {
      return 'تسویه';
    } else {
      return 'نامشخص';
    }
  }

  Status getStatus(int amount) {
    if (amount < 0) {
      return Status.deptor;
    } else if (amount > 0) {
      return Status.creditor;
    } else {
      return Status.clear;
    }
  }

  Color getStatusColor(int amount) {
    if (amount < 0)
      return Colors.red;
    else if (amount > 0)
      return Colors.green;
    else if (amount == 0)
      return Colors.yellow.shade600;
    else
      return Colors.grey;
  }
}
