import 'package:dartz/dartz.dart';
import 'package:iAmCalculatorFlutter/core/error/failures.dart';
import 'package:iAmCalculatorFlutter/features/trade/domain/entities/trade.dart';

class TradeCalculator {
  Either<Failure, List<Trade>> calculateTrades(List<Trade> trades) {
    int remnant = 0;
    List<Trade> finalTrades = [];
    List<Trade> sortedTrades = _sortTrades(trades);
    try {
      sortedTrades.forEach((trade) {
        // if (!(trade.amount is int)) {
        //   throw FormatException;
        // }
        final int amount = trade.isDebtor == 1 ? -(trade.amount) : trade.amount;
        remnant += amount;
        final Trade _trade = Trade(
            id: trade.id,
            name: trade.name,
            isDebtor: trade.isDebtor,
            amount: amount,
            amountStr: trade.amountStr,
            date: trade.date,
            userId: trade.userId,
            remnant: remnant);
        finalTrades.add(_trade);
      });
      return Right(finalTrades);
    } catch (e) {
      return Left(e);
    }
  }

  int calculateRemnant(List<Trade> trades) {
    int remnant = 0;
    if (trades.length == 1) {
      final singleTrade =
          trades[0].isDebtor == 1 ? -(trades[0].amount) : trades[0].amount;
      return singleTrade;
    }
    try {
      trades.forEach((trade) {
        if (!(trade.amount is int)) {
          throw FormatException;
        }
        final int amount = trade.isDebtor == 1 ? -(trade.amount) : trade.amount;
        remnant += amount;
      });
      return remnant;
    } catch (e) {
      throw UnimplementedError();
    }
  }

  List<Trade> _sortTrades(List<Trade> t) {
    List sorted = t;
    sorted.sort((a, b) {
      // if (a.date == null || b.date == null) {
      //   return 0;
      // }
      DateTime first = DateTime.parse(a.date);
      DateTime second = DateTime.parse(b.date);
      return first.compareTo(second);
    });
    return sorted;
  }
}
