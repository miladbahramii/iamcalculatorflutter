import 'package:dartz/dartz.dart';
import 'package:iAmCalculatorFlutter/core/error/failures.dart';
import 'package:intl/intl.dart';
import 'package:persian_date/persian_date.dart';

class InputConvertor {
  static final tomanFormater = new NumberFormat("#,##0", "fa_IR");

  Either<Failure, int> stringToUnsignedInteger(String str) {
    try {
      final integer = int.parse(str);
      if (integer < 0) throw FormatException();
      return Right(integer);
    } on FormatException {
      return Left(InvalidInputFailure());
    }
  }

  String numberToCurrency(int n) {
    return tomanFormater.format(n);
  }

  String stringToCurrency(String str) {
    final integer = int.parse(str);
    if (integer >= 0) {
      return tomanFormater.format(integer);
    } else {
      return '0';
    }
  }

  String dateToPersianText(String date) {
    final persianDate = PersianDate(gregorian: date);

    return '${persianDate.day.toString()}  ${persianDate.monthname} ${persianDate.year.toString()}';
  }
}
