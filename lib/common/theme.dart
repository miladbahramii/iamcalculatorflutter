// Copyright 2019 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';

final appTheme = ThemeData(
  primarySwatch: Colors.blue,
  fontFamily: 'IranSans',
  // pageTransitionsTheme: PageTransitionsTheme(builders: {TargetPlatform.android: CupertinoPageTransitionsBuilder(),}),
  textTheme: TextTheme(
    display4: TextStyle(
      fontWeight: FontWeight.w400,
      fontFamily: 'IranSans',
      fontSize: 24,
      color: Colors.white,
    ),
  ),
);
