import 'package:flutter/material.dart';
import 'package:iAmCalculatorFlutter/features/user/presentation/pages/user_page.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:iAmCalculatorFlutter/common/theme.dart';
import 'injection_container.dart' as di;

void main() async {
  di.init();
  // FlutterError.onError = (FlutterErrorDetails details) {
  //   FlutterError.dumpErrorToConsole(details);
  //   if (kReleaseMode)
  //     exit(1);
  // };
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // Using MultiProvider is convenient when providing multiple objects.
    return MaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        Locale("fa", "IR"),
        Locale("en", "US"),
      ],
      locale: Locale("fa", "IR"),
      title: 'Provider Demo',
      theme: appTheme,
      home: UserPage(),
    );
  }
}
