import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class Trade extends Equatable {
  final int id;
  final String name;
  final int amount;
  final int isDebtor;
  final String date;
  final int userId;
  final int remnant;
  final String amountStr;

  Trade(
      {this.id,
      @required this.name,
      @required this.isDebtor,
      this.date,
      this.amount,
      this.userId,
      this.amountStr,
      this.remnant});

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'amount': amount,
      'isDebtor': isDebtor,
      'date': date,
      'userId': userId,
    };
  }

  @override
  List<Object> get props =>
      [id, name, isDebtor, date, amount, userId, amountStr, remnant];
}
