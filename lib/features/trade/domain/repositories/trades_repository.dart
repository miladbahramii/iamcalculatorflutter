import 'package:dartz/dartz.dart';
import 'package:iAmCalculatorFlutter/core/entities/state_act_status.dart';
import 'package:iAmCalculatorFlutter/core/error/failures.dart';
import 'package:iAmCalculatorFlutter/features/trade/domain/entities/trade.dart';

abstract class TradeRepository {
  Future<Either<Failure, List<Trade>>> getTrades(int userId);

  Future<Either<Failure, StateActStatus>> addTrade(
      int userId, String name, String date, int isDebtor, int amount);

  Future<Either<Failure, StateActStatus>> deleteTrade(int id);

  Future<Either<Failure, StateActStatus>> updateTrade(
      int id, int userId, String name, String date, int isDebtor, int amount);

  Future<Either<Failure, List<Trade>>> calcTrades(List<Trade> trades);
}
