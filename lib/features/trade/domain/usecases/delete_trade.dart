import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:iAmCalculatorFlutter/core/entities/state_act_status.dart';
import 'package:iAmCalculatorFlutter/core/error/failures.dart';
import 'package:iAmCalculatorFlutter/core/usecases/usecase.dart';
import 'package:iAmCalculatorFlutter/features/trade/domain/repositories/trades_repository.dart';

class DeleteTrade implements UseCase<StateActStatus, DeleteTradeParams> {
  TradeRepository repository;
  DeleteTrade(this.repository);

  @override
  Future<Either<Failure, StateActStatus>> call(DeleteTradeParams params) async {
    return await repository.deleteTrade(params.id);
  }
}

class DeleteTradeParams extends Equatable {
  final int id;

  DeleteTradeParams({@required this.id});

  @override
  List<Object> get props => [id];
}
