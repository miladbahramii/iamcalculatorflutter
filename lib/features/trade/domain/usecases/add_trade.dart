import 'package:iAmCalculatorFlutter/core/entities/state_act_status.dart';
import 'package:iAmCalculatorFlutter/core/error/failures.dart';
import 'package:dartz/dartz.dart';
import 'package:iAmCalculatorFlutter/core/usecases/usecase.dart';
import 'package:iAmCalculatorFlutter/features/trade/domain/repositories/trades_repository.dart';
import 'package:meta/meta.dart';

class AddTrade extends UseCase<StateActStatus, AddTradeParams> {
  final TradeRepository repository;

  AddTrade(this.repository);

  @override
  Future<Either<Failure, StateActStatus>> call(AddTradeParams params) async {
    return await repository.addTrade(params.userId, params.name, params.date,
        params.isDebtor, params.amount);
  }
}

class AddTradeParams {
  final int userId;
  final String name;
  final String date;
  final int isDebtor;
  final int amount;

  AddTradeParams(
      {@required this.name,
      @required this.userId,
      @required this.date,
      @required this.isDebtor,
      @required this.amount});
}
