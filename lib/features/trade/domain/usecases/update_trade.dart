import 'package:dartz/dartz.dart';
import 'package:iAmCalculatorFlutter/core/entities/state_act_status.dart';
import 'package:iAmCalculatorFlutter/core/error/failures.dart';
import 'package:iAmCalculatorFlutter/core/usecases/usecase.dart';
import 'package:iAmCalculatorFlutter/features/trade/domain/repositories/trades_repository.dart';
import 'package:meta/meta.dart';

class UpdateTrade extends UseCase<StateActStatus, UpdateTradeParams> {
  final TradeRepository repository;
  UpdateTrade(this.repository);

  @override
  Future<Either<Failure, StateActStatus>> call(UpdateTradeParams params) async {
    return await repository.updateTrade(params.id, params.userId, params.name,
        params.date, params.isDebtor, params.amount);
  }
}

class UpdateTradeParams {
  final int id;
  final int userId;
  final String name;
  final int amount;
  final String date;
  final int isDebtor;

  UpdateTradeParams({
    @required this.id,
    @required this.userId,
    @required this.name,
    @required this.amount,
    @required this.date,
    @required this.isDebtor,
  });
}
