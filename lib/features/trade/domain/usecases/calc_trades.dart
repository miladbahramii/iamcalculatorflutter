import 'package:iAmCalculatorFlutter/core/error/failures.dart';
import 'package:dartz/dartz.dart';
import 'package:iAmCalculatorFlutter/core/usecases/usecase.dart';
import 'package:iAmCalculatorFlutter/features/trade/domain/entities/trade.dart';
import 'package:iAmCalculatorFlutter/features/trade/domain/repositories/trades_repository.dart';

class CalcTrades extends UseCase<List<Trade>, CalcTradeParams> {

  final TradeRepository tradeRepository;

  CalcTrades(this.tradeRepository);

  @override
  Future<Either<Failure, List<Trade>>> call(params) async{
    return await tradeRepository.calcTrades(params.trades);
  }
}

class CalcTradeParams {
  final List<Trade> trades;

  CalcTradeParams(this.trades);
}