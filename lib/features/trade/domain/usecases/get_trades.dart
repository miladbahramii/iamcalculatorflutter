import 'package:dartz/dartz.dart';
import 'package:iAmCalculatorFlutter/core/error/failures.dart';
import 'package:iAmCalculatorFlutter/core/usecases/usecase.dart';
import 'package:iAmCalculatorFlutter/features/trade/domain/entities/trade.dart';
import 'package:iAmCalculatorFlutter/features/trade/domain/repositories/trades_repository.dart';

class GetTrades implements UseCase<List<Trade>, GetTradesParams> {
  final TradeRepository repository;
  GetTrades(this.repository);

  @override
  Future<Either<Failure, List<Trade>>> call(GetTradesParams params) async =>
      await repository.getTrades(params.userId);
}

class GetTradesParams {
  final int userId;

  GetTradesParams({this.userId});
}
