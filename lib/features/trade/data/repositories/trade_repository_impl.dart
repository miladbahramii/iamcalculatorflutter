import 'package:dartz/dartz.dart';
import 'package:iAmCalculatorFlutter/core/entities/state_act_status.dart';
import 'package:iAmCalculatorFlutter/core/error/exeptions.dart';
import 'package:iAmCalculatorFlutter/core/error/failures.dart';
import 'package:iAmCalculatorFlutter/core/network/network_info.dart';
import 'package:iAmCalculatorFlutter/features/trade/data/datasources/trade_local_data_source.dart';
import 'package:iAmCalculatorFlutter/features/trade/domain/entities/trade.dart';
import 'package:iAmCalculatorFlutter/features/trade/domain/repositories/trades_repository.dart';
import 'package:meta/meta.dart';

typedef Future<StateActStatus> _DatabaseAction();

class TradeRepositoryImpl implements TradeRepository {
  final TradeLocalDataSource localDataSource;
  final NetworkInfo networkInfo;

  TradeRepositoryImpl(
      {@required this.localDataSource, @required this.networkInfo});

  @override
  Future<Either<Failure, List<Trade>>> getTrades(int userId) async {
    try {
      final localTrades = await localDataSource.getTrades(userId);
      return Right(localTrades);
    } on DatabaseExeption {
      return Left(LocalDatabaseFailure());
    }
  }

  @override
  Future<Either<Failure, StateActStatus>> deleteTrade(int id) async {
    return await _databaseAction('مشتری با موفقیت حذف شد.', () {
      return localDataSource.deleteTrade(id);
    });
  }

  @override
  Future<Either<Failure, StateActStatus>> updateTrade(int id, int userId,
      String name, String date, int isDebtor, int amount) async {
    return await _databaseAction('مشتری با موفقیت بروز رسانی شد.', () {
      final trade = Trade(
        id: id,
        name: name,
        date: date,
        isDebtor: isDebtor,
        amount: amount,
        userId: userId,
      );
      return localDataSource.updateTrade(trade);
    });
  }

  @override
  Future<Either<Failure, StateActStatus>> addTrade(
      int userId, String name, String date, int isDebtor, int amount) async {
    return await _databaseAction('مشتری با موفقیت افزوده شد.', () {
      final trade = Trade(
        userId: userId,
        name: name,
        date: date,
        isDebtor: isDebtor,
        amount: amount,
      );
      return localDataSource.addTrade(trade);
    });
  }

  Future<Either<Failure, StateActStatus>> _databaseAction(
      String msg, _DatabaseAction callAction) async {
    try {
      callAction();
      return Right(StateActStatus(message: msg, completed: true));
    } on DatabaseExeption {
      return Left(LocalDatabaseFailure());
    }
  }

  @override
  Future<Either<Failure, List<Trade>>> calcTrades(List<Trade> trades) {}
}
