import 'package:iAmCalculatorFlutter/features/trade/domain/entities/trade.dart';
import 'package:meta/meta.dart';

class TradeModel extends Trade {
  final int id;
  final String name;
  final int amount;
  final int isDebtor;
  final String date;
  final int userId;

  TradeModel(
      {this.id,
      this.name,
      this.amount,
      this.isDebtor,
      this.date,
      this.userId})
      : super(
            id: id,
            name: name,
            amount: amount,
            isDebtor: isDebtor,
            date: date,
            userId: userId);

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'amount': amount,
      'isDebtor': isDebtor,
      'date': date,
      'userId': userId,
    };
  }

  factory TradeModel.fromJson(Map<String, dynamic> json) {
    return TradeModel(
      id: json['id'],
      name: json['name'],
      amount: json['amount'],
      date: json['date'],
      isDebtor: json['isDebtor'],
      userId: json['userId'],
    );
  }
}
