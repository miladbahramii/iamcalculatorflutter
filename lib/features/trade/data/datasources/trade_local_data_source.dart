import 'package:iAmCalculatorFlutter/core/entities/state_act_status.dart';
import 'package:iAmCalculatorFlutter/features/trade/data/models/trade_model.dart';
import 'package:iAmCalculatorFlutter/features/trade/domain/entities/trade.dart';
import 'package:iAmCalculatorFlutter/features/trade/domain/usecases/add_trade.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:meta/meta.dart';

abstract class TradeLocalDataSource {
  Future<List<TradeModel>> getTrades(int userId);
  Future<StateActStatus> addTrade(Trade trade);
  Future<StateActStatus> deleteTrade(int id);
  Future<StateActStatus> updateTrade(Trade trade);
}

class TradeLocalDataSourceImpl implements TradeLocalDataSource {
  Future<Database> database;

  TradeLocalDataSourceImpl() {
    initialDatabase();
  }

  Future<Database> initialDatabase() async {
    return openDatabase(
      join(await getDatabasesPath(), 'calcbook_trades.db'),
      onCreate: (db, version) {
        return db.execute(
          "CREATE TABLE trades(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, amount INTEGER, isDebtor Integer, date TEXT, userId INTEGER)",
        );
      },
      version: 1,
    );
  }

  @override
  Future<StateActStatus> addTrade(Trade trade) async {
    final db = await initialDatabase();
    final _trade = TradeModel(
      userId: trade.userId,
      name: trade.name,
      amount: trade.amount,
      isDebtor: trade.isDebtor,
      date: trade.date,
    );
    if (db != null) {
      await db.insert(
        'trades',
        _trade.toJson(),
        conflictAlgorithm: ConflictAlgorithm.replace,
      );
    }

    return StateActStatus(
        completed: true, message: 'معامله جدید با موفقیت افزوده شد');
  }

  @override
  Future<StateActStatus> deleteTrade(int id) async {
    final db = await initialDatabase();
    if (db != null) {
      await db.delete(
        'trades',
        where: "id = ?",
        whereArgs: [id],
      );
    }
    return StateActStatus(completed: true, message: 'معامله با موفقیت حذف شد');
  }

  @override
  Future<List<TradeModel>> getTrades(int userId) async {
    final db = await initialDatabase();
    final List<Map<String, dynamic>> tradesList =
        await db.rawQuery('SELECT * FROM trades WHERE userId = $userId');
    return List.generate(tradesList.length, (i) {
      return TradeModel.fromJson(tradesList[i]);
    });
  }

  @override
  Future<StateActStatus> updateTrade(Trade trade) async {
    final db = await initialDatabase();
    final _trade = TradeModel(
        name: trade.name,
        amount: trade.amount,
        isDebtor: trade.isDebtor,
        date: trade.date,
        id: trade.id,
        userId: trade.userId);
    if (db != null) {
      await db.update(
        'trades',
        _trade.toJson(),
        where: "id = ?",
        whereArgs: [trade.id],
      );
    }
    return StateActStatus(
        completed: true, message: 'معامله با موفقیت بروزرسانی شد');
  }
}
