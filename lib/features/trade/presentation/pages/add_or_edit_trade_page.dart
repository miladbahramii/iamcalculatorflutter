import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:iAmCalculatorFlutter/features/trade/domain/entities/trade.dart';
import 'package:iAmCalculatorFlutter/features/trade/presentation/cubit/trade_cubit.dart';
import 'package:iAmCalculatorFlutter/features/trade/presentation/pages/trade_page.dart';
import 'package:iAmCalculatorFlutter/features/trade/presentation/widgets/trade_form.dart';
import 'package:iAmCalculatorFlutter/features/user/domain/entities/user.dart';
import '../../../../injection_container.dart';

class AddOrEditTradePage extends StatelessWidget {
  final User user;
  final Trade trade;

  const AddOrEditTradePage({Key key, this.user, this.trade}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(trade != null ? 'ویرایش معامله' : 'افزودن معامله'),
      ),
      // bottomNavigationBar: _bottomNavigationBuilder(),
      body: BlocProvider<TradeCubit>(
        create: (_) => sl(),
        child: buildBody(context),
      ),
    );
  }

  Widget buildBody(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => TradePage(
                    user: user,
                  )),
        );
        return false;
      },
      child: TradeForm(
        user: user,
        trade: trade,
      ),
    );
  }
}
