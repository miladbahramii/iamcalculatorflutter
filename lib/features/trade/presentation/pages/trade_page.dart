import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:iAmCalculatorFlutter/core/colors/palette.dart';
import 'package:iAmCalculatorFlutter/core/entities/state_act_status.dart';
import 'package:iAmCalculatorFlutter/features/trade/presentation/cubit/trade_cubit.dart';
import 'package:iAmCalculatorFlutter/features/trade/presentation/pages/add_or_edit_trade_page.dart';
import 'package:iAmCalculatorFlutter/features/trade/presentation/widgets/widgets.dart';
import 'package:iAmCalculatorFlutter/features/user/domain/entities/user.dart';
import 'package:iAmCalculatorFlutter/features/user/presentation/pages/user_page.dart';
import 'package:iAmCalculatorFlutter/injection_container.dart';

class TradePage extends StatelessWidget {
  final StateActStatus initialSnackbar;
  final User user;

  const TradePage({Key key, this.initialSnackbar, this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('معاملات'),
        backgroundColor: Palette.primary,
      ),
      body: BlocProvider(
        create: (context) => sl<TradeCubit>(),
        child: TradesDisplay(initialSnackbar: initialSnackbar, user: user),
      ),
      bottomNavigationBar: _bottomNavigation(context),
    );
  }

  Widget _bottomNavigation(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => UserPage()),
        );
        return false;
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: RaisedButton(
            color: Palette.primary,
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => AddOrEditTradePage(
                          user: user,
                        )),
              );
            },
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 12),
              child: Text(
                'افزودن معامله',
                style: TextStyle(color: Colors.white),
              ),
            )),
      ),
    );
  }
}
