import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:iAmCalculatorFlutter/core/entities/state_act_status.dart';
import 'package:iAmCalculatorFlutter/core/error/failures.dart';
import 'package:iAmCalculatorFlutter/core/utils/input_convertor.dart';
import 'package:iAmCalculatorFlutter/core/utils/trade_calculator.dart';
import 'package:iAmCalculatorFlutter/features/trade/domain/entities/trade.dart';
import 'package:iAmCalculatorFlutter/features/trade/domain/usecases/usecases.dart';
import 'package:iAmCalculatorFlutter/features/user/domain/entities/user.dart';
import 'package:iAmCalculatorFlutter/features/user/domain/usecases/usecases.dart';
import 'package:meta/meta.dart';
import 'package:dartz/dartz.dart';

part 'trade_state.dart';

const String DATABASE_FAILURE_MESSAGE = 'Database Failure';
const String INVALID_INPUT_FAILURE_MESSAGE =
    'Invalid Input - The number must be a positive integer or zero.';

class TradeCubit extends Cubit<TradeState> {
  final AddTrade addTrade;
  final UpdateUser updateUser;
  final UpdateTrade updateTrade;
  final DeleteTrade deleteTrade;
  final GetTrades getTrades;
  final InputConvertor inputConvertor;
  final TradeCalculator tradeCalculator;

  TradeCubit(
      {@required this.addTrade,
      @required this.updateTrade,
      @required this.deleteTrade,
      @required this.getTrades,
      @required this.inputConvertor,
      @required this.tradeCalculator,
      @required this.updateUser})
      : super(Empty());

  void initialTrades(int userId) async {
    emit(Loading());
    final trades = await getTrades(GetTradesParams(userId: userId));
    _tradesLoadedOrErrorState(trades);
  }

  void searchInTrades(List<Trade> trades, String searchStr, int userId) async {
    emit(Loading());
    if (searchStr.length > 0 && trades != null) {
      final List<Trade> _searchedTrades =
          trades.where((element) => element.name.contains(searchStr)).toList();
      emit(TradeLoaded(trades: _searchedTrades));
    } else {
      initialTrades(userId);
    }
  }

  void amountMonitoring(String amount) {
    final _a = inputConvertor.stringToUnsignedInteger(amount);
    _a.fold((l) {
      emit(AmountMonitoring('0'));
    }, (integer) {
      final finalAmount = inputConvertor.numberToCurrency(integer);
      emit(AmountMonitoring(finalAmount));
    });
  }

  Future<void> insertTrade(Trade trade, User user) async {
    emit(Loading());
    final _amount = inputConvertor.stringToUnsignedInteger(trade.amountStr);
    _amount
        .fold((failure) => emit(Error(message: _mapFailureToMessage(failure))),
            (integer) async {
      final actionReport = await addTrade(AddTradeParams(
          name: trade.name,
          amount: integer,
          date: trade.date,
          isDebtor: trade.isDebtor,
          userId: trade.userId));
      final StateActStatus stateActStatus =
          await _eitherLoadedOrErrorState(actionReport);
      if (stateActStatus is StateActStatus) {
        await _updateUserRemnant(user);
        emit(StatusLoaded(
            message: stateActStatus.message,
            completed: stateActStatus.completed));
      } else {
        throw Error(message: 'unexpected error');
      }
    });
  }

  void removeTrade(Trade trade, User user) async {
    final actionReport = await deleteTrade(DeleteTradeParams(id: trade.id));
    final StateActStatus stateActStatus =
        await _eitherLoadedOrErrorState(actionReport);
    if (stateActStatus is StateActStatus) {
      initialTrades(trade.userId);
      _updateUserRemnant(user);
      emit(StatusLoaded(
          completed: stateActStatus.completed,
          message: stateActStatus.message));
    } else {
      throw Error(message: 'unexpected error');
    }
  }

  void editTrade(Trade trade, User user) async {
    emit(Loading());
    final _amount = inputConvertor.stringToUnsignedInteger(trade.amountStr);
    _amount
        .fold((failure) => emit(Error(message: _mapFailureToMessage(failure))),
            (integer) async {
      final actionReport = await updateTrade(UpdateTradeParams(
          name: trade.name,
          amount: integer,
          date: trade.date,
          id: trade.id,
          userId: trade.userId,
          isDebtor: trade.isDebtor));

      final StateActStatus stateActStatus =
          await _eitherLoadedOrErrorState(actionReport);

      if (stateActStatus is StateActStatus) {
        _updateUserRemnant(user);
        emit(StatusLoaded(
            message: stateActStatus.message,
            completed: stateActStatus.completed));
      } else {
        throw Error(message: 'unexpected error');
      }
    });
  }

  Future<void> _updateUserRemnant(User user) async {
    final _t = await getTrades(GetTradesParams(userId: user.id));
    int remnant;
    _t.fold((failure) => null, (trades) {
      remnant = tradeCalculator.calculateRemnant(trades);
    });
    updateUser(UpdateParams(
        remnant: remnant,
        id: user.id,
        address: user.address,
        name: user.name,
        phone: user.phone));
  }

  Future<StateActStatus> _eitherLoadedOrErrorState(
    Either<Failure, StateActStatus> failureOrMessage,
  ) async {
    StateActStatus _s;

    failureOrMessage.fold(
      (failure) {
        _s = StateActStatus(
            completed: false, message: _mapFailureToMessage(failure));
      },
      (payload) {
        _s = StateActStatus(
            completed: payload.completed, message: payload.message);
      },
    );

    return _s;
  }

  _tradesLoadedOrErrorState(Either<Failure, List<Trade>> failureOrTrades) {
    failureOrTrades.fold(
      (failure) {
        emit(Error(message: _mapFailureToMessage(failure)));
      },
      (_t) {
        final finalTrades = tradeCalculator.calculateTrades(_t);
        finalTrades.fold(
            (failure) => emit(Error(message: _mapFailureToMessage(failure))),
            (trades) {
          trades.length > 0 ? emit(TradeLoaded(trades: trades)) : emit(Empty());
        });
      },
    );
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case LocalDatabaseFailure:
        return DATABASE_FAILURE_MESSAGE;
      default:
        return 'undefind error';
    }
  }
}
