part of 'trade_cubit.dart';

abstract class TradeState extends Equatable {
  const TradeState();
}

class Empty extends TradeState {
  @override
  List<Object> get props => [];
}

class TradeLoaded extends TradeState {
  final List<Trade> trades;
  final String statusMessage;
  final bool isComplete;

  TradeLoaded({@required this.trades, this.statusMessage, this.isComplete});

  @override
  List<Object> get props => [trades, statusMessage, isComplete];
}

class StatusLoaded extends TradeState {
  final String message;
  final bool completed;

  StatusLoaded({@required this.message, @required this.completed});

  @override
  List<Object> get props => [message, completed];
}

class Loading extends TradeState {
  @override
  List<Object> get props => [];
}

class AmountMonitoring extends TradeState {
  final String amount;

  AmountMonitoring(this.amount);

  @override
  List<Object> get props => [amount];
}

class Error extends TradeState {
  final String message;

  Error({@required this.message});

  @override
  List<Object> get props => [];
}
