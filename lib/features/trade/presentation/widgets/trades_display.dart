import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:iAmCalculatorFlutter/core/colors/palette.dart';
import 'package:iAmCalculatorFlutter/core/entities/state_act_status.dart';
import 'package:iAmCalculatorFlutter/core/utils/input_convertor.dart';
import 'package:iAmCalculatorFlutter/core/widgets/currency_badge.dart';
import 'package:iAmCalculatorFlutter/features/trade/domain/entities/trade.dart';
import 'package:iAmCalculatorFlutter/features/trade/presentation/cubit/trade_cubit.dart';
import 'package:iAmCalculatorFlutter/features/trade/presentation/pages/add_or_edit_trade_page.dart';
import 'package:iAmCalculatorFlutter/features/trade/presentation/widgets/loading_widget.dart';
import 'package:iAmCalculatorFlutter/features/trade/presentation/widgets/message_display.dart';
import 'package:iAmCalculatorFlutter/features/trade/presentation/widgets/trade_table.dart';
import 'package:iAmCalculatorFlutter/features/user/domain/entities/user.dart';
import 'package:iAmCalculatorFlutter/injection_container.dart';
import 'package:persian_date/persian_date.dart';

class TradesDisplay extends StatefulWidget {
  final StateActStatus initialSnackbar;
  final User user;
  const TradesDisplay({Key key, this.initialSnackbar, this.user})
      : super(key: key);

  @override
  _TradesDisplayState createState() => _TradesDisplayState();
}

class _TradesDisplayState extends State<TradesDisplay> {
  List<Trade> tradesList;

  @override
  void initState() {
    super.initState();
    context.bloc<TradeCubit>().initialTrades(widget.user.id);

    if (widget.initialSnackbar != null) {
      showSnackbar(
          widget.initialSnackbar.message, widget.initialSnackbar.completed);
    }

    context.bloc<TradeCubit>().listen((state) {
      if (state is TradeLoaded && tradesList == null) {
        tradesList = state.trades;
      } else if (state is StatusLoaded) {
        showSnackbar(state.message, state.completed);
      }
    });
  }

  void searchTrades(String value) {
    context
        .bloc<TradeCubit>()
        .searchInTrades(tradesList, value, widget.user.id);
  }

  showSnackbar(String message, bool isComplete) async {
    Scaffold.of(context).hideCurrentSnackBar();
    final snackBar = SnackBar(
      backgroundColor: isComplete ? Colors.green : Colors.red,
      content: Text(message,
          style: TextStyle(
            fontSize: 14,
            fontFamily: 'IranSans',
          )),
    );

    Future.delayed(Duration.zero, () {
      Scaffold.of(context).showSnackBar(snackBar);
    });
  }

  removeTrade(Trade trade) {
    Navigator.pop(context);
    context.bloc<TradeCubit>().removeTrade(trade, widget.user);
  }

  void onTapTrade(Trade trade) {
    final isDebtorText = trade.isDebtor == 1 ? 'بدهکار' : 'بستانکار';
    final perisanDate = sl<InputConvertor>().dateToPersianText(trade.date);

    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) => AlertDialog(
              title: Text(trade.name),
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[
                    Text('مشتری: ${widget.user.name}'),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      '$isDebtorText: ${trade.amount.toString()} تومان',
                      style: TextStyle(fontFamily: 'IranSansNumber'),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text('تاریخ: $perisanDate'),
                  ],
                ),
              ),
              actions: <Widget>[
                FlatButton(
                  child: Text('ویرایش'),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => AddOrEditTradePage(
                              trade: trade, user: widget.user),
                        ));
                  },
                ),
                FlatButton(
                  child: Text('حذف'),
                  onPressed: () => removeTrade(trade),
                ),
                FlatButton(
                  child: Text('انصراف'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ));
  }

  @override
  Widget build(BuildContext _context) {
    return Container(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: TextFormField(
                onChanged: (value) => searchTrades(value),
                decoration: const InputDecoration(
                  hintStyle: TextStyle(fontSize: 16),
                  contentPadding:
                      EdgeInsets.symmetric(vertical: 0, horizontal: 15),
                  hintText: 'جستجو معامله',
                  alignLabelWithHint: true,
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.blue,
                      width: 1,
                    ),
                    borderRadius: const BorderRadius.all(Radius.circular(50)),
                  ),
                  border: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.grey,
                      width: 1,
                    ),
                    borderRadius: const BorderRadius.all(Radius.circular(50)),
                  ),
                )),
          ),
          Expanded(
            child: BlocBuilder<TradeCubit, TradeState>(
              builder: (BuildContext context, state) {
                if (state is TradeLoaded) {
                  if (state.statusMessage != null) {
                    showSnackbar(state.statusMessage, state.isComplete);
                  }
                  return TradeTable(
                    trades: state.trades,
                    onTapData: onTapTrade,
                  );
                } else if (state is Loading) {
                  return LoadingWidget();
                }
                return MessageDisplay(
                  message: 'هیچ معامله ای یافت نشد.',
                );
              },
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
            child: BlocBuilder<TradeCubit, TradeState>(
              builder: (BuildContext context, state) {
                if (state is TradeLoaded) {
                  final amount = state.trades[state.trades.length - 1].remnant;
                  return Row(
                    children: [
                      Expanded(
                        child: Center(
                          child: Text(
                            "مانده:",
                            textDirection: TextDirection.rtl,
                            style: TextStyle(
                                color: Palette.dark,
                                fontFamily: "IranSansNumber",
                                fontSize: 16,
                                fontWeight: FontWeight.bold),
                            textAlign: TextAlign.start,
                          ),
                        ),
                      ),
                      Expanded(
                        child: Center(
                          child: Row(
                            children: [
                              Text(
                                '${sl<InputConvertor>().numberToCurrency(amount)}',
                                style: TextStyle(
                                    color: Palette.dark,
                                    fontFamily: "IranSansNumber",
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold),
                                textAlign: TextAlign.start,
                                textDirection: TextDirection.ltr,
                              ),
                              SizedBox(width: 5),
                              CurrencyBadge(
                                amount: amount,
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  );
                }
                return Container();
              },
            ),
          )
        ],
      ),
    );
  }
}
