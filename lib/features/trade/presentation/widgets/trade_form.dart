import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:iAmCalculatorFlutter/core/colors/palette.dart';
import 'package:iAmCalculatorFlutter/core/entities/state_act_status.dart';
import 'package:iAmCalculatorFlutter/core/utils/input_convertor.dart';
import 'package:iAmCalculatorFlutter/core/widgets/currency_widget.dart';
import 'package:iAmCalculatorFlutter/core/widgets/radio_box.dart';
import 'package:iAmCalculatorFlutter/features/trade/domain/entities/trade.dart';
import 'package:iAmCalculatorFlutter/features/trade/presentation/cubit/trade_cubit.dart';
import 'package:iAmCalculatorFlutter/features/trade/presentation/pages/trade_page.dart';
import 'package:iAmCalculatorFlutter/features/user/domain/entities/user.dart';
import 'package:iAmCalculatorFlutter/injection_container.dart';
import 'package:jalali_calendar/jalali_calendar.dart';

enum Fields { name, amount }

class TradeForm extends StatefulWidget {
  final User user;
  final Trade trade;
  const TradeForm({
    this.user,
    this.trade,
    Key key,
  }) : super(key: key);

  @override
  _TradeFormState createState() => _TradeFormState();
}

class _TradeFormState extends State<TradeForm> {
  String _name = "";
  String _amount = "";
  int _isDebtor;
  String _date;
  String _amountStr = '0';
  String _buttonDateText = 'انتخاب تاریخ';
  final _tradeFormKey = GlobalKey<FormState>();

  final _nameFieldController = TextEditingController();
  final _amountFieldController = TextEditingController();
  final _nameFocusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    if (widget.trade != null) {
      _name = widget.trade.name;
      _amount = widget.trade.amount.toString();
      _date = widget.trade.date;
      _buttonDateText = sl<InputConvertor>().dateToPersianText(_date);
      _nameFieldController.text = _name;
      _amountFieldController.text =
          int.parse(widget.trade.amount.toString()).abs().toString();
      setState(() {
        _isDebtor = widget.trade.isDebtor;
      });
      amountMonitoring(int.parse(_amount).abs().toString());
    } else {
      _isDebtor = 1;
      _date = DateTime.now().toString();
      _buttonDateText = sl<InputConvertor>().dateToPersianText(_date);
    }

    listenToCatchState();
  }

  void listenToCatchState() {
    context.bloc<TradeCubit>().listen((state) {
      if (state is StatusLoaded) {
        if (widget.trade != null) {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => TradePage(
                  initialSnackbar: StateActStatus(
                      completed: state.completed, message: state.message),
                  user: widget.user,
                ),
              ));
        } else {
          showToast(state.message);
          resetForm();
        }
      }
    });
  }

  void showToast(String message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        // gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.green,
        textColor: Colors.white,
        fontSize: 18.0);
  }

  void resetForm() {
    _name = '';
    _amount = '';
    setState(() {
      _amountStr = '0';
      _isDebtor = 1;
      _date = DateTime.now().toString();
      _buttonDateText = sl<InputConvertor>().dateToPersianText(_date);
    });

    _nameFieldController.clear();
    _amountFieldController.clear();
    _nameFocusNode.requestFocus();
  }

  onChangeField(String text, Fields fieldName) {
    switch (fieldName) {
      case Fields.name:
        _name = text;
        break;
      case Fields.amount:
        _amount = text;
        amountMonitoring(_amount);
        break;
      default:
        throw UnimplementedError();
    }
  }

  void amountMonitoring(String amount) {
    final a = sl<InputConvertor>().stringToCurrency(amount);

    setState(() {
      _amountStr = a;
    });
  }

  submitForm() async {
    if (!_tradeFormKey.currentState.validate()) return 0;
    final amountUnsigned = int.parse(_amount).abs().toString();
    Trade trade;
    if (widget.trade != null) {
      trade = Trade(
          name: _name,
          amountStr: amountUnsigned,
          date: _date,
          isDebtor: _isDebtor,
          userId: widget.user.id,
          id: widget.trade.id);
    } else {
      trade = Trade(
        name: _name,
        amountStr: amountUnsigned,
        date: _date,
        isDebtor: _isDebtor,
        userId: widget.user.id,
      );
    }
    if (widget.trade != null) {
      context.bloc<TradeCubit>().editTrade(trade, widget.user);
    } else
      context.bloc<TradeCubit>().insertTrade(trade, widget.user);
  }

  void showSnackbar(String message, bool isCompleted) {
    Scaffold.of(context).hideCurrentSnackBar();
    final snackBar = SnackBar(
      backgroundColor: isCompleted ? Colors.green : Colors.red,
      content: Text(
        message,
        style: TextStyle(fontFamily: 'IranSans'),
      ),
    );
    Scaffold.of(context).showSnackBar(snackBar);
  }

  formValidator(String value, Fields field) {
    final _s = field == Fields.amount ? 'مبلغ' : 'نام';
    if (value.isEmpty) {
      return 'یک $_s نظر بگیرید.';
    }
  }

  handleRadioChange(value) {
    setState(() {
      _isDebtor = value;
    });
  }

  _pickDate() async {
    FocusScope.of(context).unfocus();
    _date =
        await jalaliCalendarPicker(context: context, convertToGregorian: true);
    setState(() {
      _buttonDateText = sl<InputConvertor>().dateToPersianText(_date);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15),
      child: Column(
        children: <Widget>[
          Expanded(
            child: Form(
              key: _tradeFormKey,
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    TextFormField(
                      controller: _nameFieldController,
                      focusNode: _nameFocusNode,
                      decoration: const InputDecoration(
                        hintText: '',
                        labelText: 'توضیحات',
                        alignLabelWithHint: true,
                      ),
                      onChanged: (text) => onChangeField(text, Fields.name),
                      validator: (value) => formValidator(value, Fields.name),
                    ),
                    TextFormField(
                      // initialValue: _amount,
                      controller: _amountFieldController,
                      keyboardType: TextInputType.numberWithOptions(
                          decimal: false, signed: false),
                      decoration: const InputDecoration(
                        hintText: '',
                        labelText: 'مبلغ',
                      ),
                      onChanged: (text) => onChangeField(text, Fields.amount),
                      validator: (value) => formValidator(value, Fields.amount),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Center(
                      child: CurrencyWidget(
                        amountStr: _amountStr,
                      ),
                    ),
                    Column(
                      children: <Widget>[
                        // ListTile(
                        //   title: const Text('بدهکار'),
                        //   leading: Radio(
                        //     value: 1,
                        //     groupValue: _isDebtor,
                        //     onChanged: handleRadioChange,
                        //   ),
                        // ),
                        RadioBox(
                          title: 'بدهکار',
                          value: 1,
                          groupValue: _isDebtor,
                          onChanged: handleRadioChange,
                        ),
                        RadioBox(
                          title: 'بستانکار',
                          value: 0,
                          groupValue: _isDebtor,
                          onChanged: handleRadioChange,
                        ),
                        ButtonTheme(
                          minWidth: MediaQuery.of(context).size.width / 2,
                          child: OutlineButton(
                            color: Palette.primary,
                            onPressed: _pickDate,
                            child: Text(_buttonDateText,
                                style: TextStyle(
                                    fontFamily: 'IranSansNumber',
                                    color: Palette.dark)),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
          Container(
            child: ButtonTheme(
              minWidth: double.infinity,
              child: RaisedButton(
                // onPressed: submitForm,
                padding: EdgeInsets.symmetric(vertical: 10),
                color: Palette.primary,
                onPressed: () => submitForm(),
                child: Text(
                  'ذخیره',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
