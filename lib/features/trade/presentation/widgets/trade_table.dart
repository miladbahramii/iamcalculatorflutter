import 'package:flutter/material.dart';
import 'package:iAmCalculatorFlutter/core/colors/palette.dart';
import 'package:iAmCalculatorFlutter/core/utils/input_convertor.dart';
import 'package:iAmCalculatorFlutter/features/trade/domain/entities/trade.dart';
import 'package:iAmCalculatorFlutter/injection_container.dart';

class TradeTable extends StatelessWidget {
  final List<Trade> trades;
  final onTapData;
  final InputConvertor inputConvertor = InputConvertor();
  TradeTable({this.trades, this.onTapData});

  Color getCurrencyColor(int currency) {
    return currency < 0 ? Colors.red : Colors.green;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: SingleChildScrollView(
        child: FittedBox(
          child: DataTable(
            columnSpacing: 10,
            showCheckboxColumn: false,
            horizontalMargin: 10,
            columns: [
              DataColumn(
                  label: Container(
                padding: EdgeInsets.only(right: 20),
                width: MediaQuery.of(context).size.width / 3,
                child: Text(
                  'عنوان',
                  style: TextStyle(
                      fontFamily: 'IranSans',
                      fontSize: 16,
                      fontWeight: FontWeight.bold),
                ),
              )),
              DataColumn(
                  label: Container(
                width: MediaQuery.of(context).size.width / 3,
                child: Center(
                  child: Text(
                    'بدهکار/بستانکار',
                    style: TextStyle(
                        fontFamily: 'IranSans',
                        fontSize: 16,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              )),
              DataColumn(
                  label: Container(
                width: MediaQuery.of(context).size.width / 3,
                child: Center(
                  child: Text(
                    'مانده',
                    style: TextStyle(
                        fontFamily: 'IranSans',
                        fontSize: 16,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              )),
            ],
            rows: trades.reversed
                .map((trade) => DataRow(
                        onSelectChanged: (bool selected) => onTapData(trade),
                        cells: [
                          DataCell(Container(
                            width: MediaQuery.of(context).size.width / 3,
                            child: Row(
                              children: [
                                Container(
                                    child: Icon(
                                  Icons.more_vert,
                                  size: 18,
                                )),
                                Center(
                                    child: Text(trade.name,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                            fontFamily: 'IranSans',
                                            fontSize: 16,
                                            color: Palette.dark))),
                              ],
                            ),
                          )),
                          DataCell(Container(
                            width: MediaQuery.of(context).size.width / 3,
                            child: Center(
                              child: Text(
                                  sl<InputConvertor>()
                                      .numberToCurrency(trade.amount),
                                  textDirection: TextDirection.ltr,
                                  style: TextStyle(
                                    color: getCurrencyColor(trade.amount),
                                    fontSize: 16,
                                    fontFamily: 'IranSansNumber',
                                  )),
                            ),
                          )),
                          DataCell(Container(
                            width: MediaQuery.of(context).size.width / 3,
                            child: Center(
                              child: Text(
                                  sl<InputConvertor>()
                                      .numberToCurrency(trade.remnant),
                                  textDirection: TextDirection.ltr,
                                  style: TextStyle(
                                    color: getCurrencyColor(trade.remnant),
                                    fontSize: 16,
                                    fontFamily: 'IranSansNumber',
                                  )),
                            ),
                          )),
                        ]))
                .toList(),
          ),
        ),
      ),
    );
  }
}
