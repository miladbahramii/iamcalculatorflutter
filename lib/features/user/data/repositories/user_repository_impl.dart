import 'package:dartz/dartz.dart';
import 'package:iAmCalculatorFlutter/core/entities/state_act_status.dart';
import 'package:iAmCalculatorFlutter/core/error/exeptions.dart';
import 'package:iAmCalculatorFlutter/core/error/failures.dart';
import 'package:iAmCalculatorFlutter/core/network/network_info.dart';
import 'package:iAmCalculatorFlutter/features/user/data/datasources/user_local_data_source.dart';
import 'package:iAmCalculatorFlutter/features/user/data/models/user_model.dart';
import 'package:iAmCalculatorFlutter/features/user/domain/entities/user.dart';
import 'package:iAmCalculatorFlutter/features/user/domain/repositories/user_repository.dart';
import 'package:meta/meta.dart';

typedef Future<StateActStatus> _DatabaseAction();

class UserRepositoryImpl implements UserRepository {
  final UserLocalDataSource localDataSource;
  final NetworkInfo networkInfo;

  UserRepositoryImpl(
      {@required this.localDataSource, @required this.networkInfo});

  @override
  Future<Either<Failure, List<UserModel>>> getUsers() async {
    try {
      final localUsers = await localDataSource.getUsers();
      return Right(localUsers);
    } on DatabaseExeption {
      return Left(LocalDatabaseFailure());
    }
  }

  @override
  Future<Either<Failure, StateActStatus>> deleteUser(int id) async {
    return await _databaseAction('مشتری با موفقیت حذف شد.', () {
      return localDataSource.deleteUser(id);
    });
  }

  @override
  Future<Either<Failure, StateActStatus>> updateUser(
      int id, String name, String address, int phone, int remnant) async {
    return await _databaseAction('مشتری با موفقیت بروز رسانی شد.', () {
      final user = User(
          id: id, name: name, address: address, phone: phone, remnant: remnant);
      return localDataSource.updateUser(user);
    });
  }

  @override
  Future<Either<Failure, StateActStatus>> addUser(
      String name, String address, int phone) async {
    return await _databaseAction('مشتری با موفقیت افزوده شد.', () {
      return localDataSource.addUser(name, address, phone);
    });
  }

  @override
  Future<Either<Failure, StateActStatus>> callUser(num userPhone) {
    // TODO: implement callUser
    throw UnimplementedError();
  }

  Future<Either<Failure, StateActStatus>> _databaseAction(
      String msg, _DatabaseAction callAction) async {
    try {
      callAction();
      return Right(StateActStatus(message: msg, completed: true));
    } on DatabaseExeption {
      return Left(LocalDatabaseFailure());
    }
  }
}
