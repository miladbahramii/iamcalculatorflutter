import 'package:iAmCalculatorFlutter/core/entities/state_act_status.dart';
import 'package:iAmCalculatorFlutter/features/user/data/models/user_model.dart';
import 'package:iAmCalculatorFlutter/features/user/domain/entities/user.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:meta/meta.dart';

abstract class UserLocalDataSource {
  Future<List<UserModel>> getUsers();
  Future<StateActStatus> addUser(String name, String address, int phone);
  Future<StateActStatus> deleteUser(int id);
  Future<StateActStatus> updateUser(User user);
  Future<StateActStatus> callUser(num phone);
}

class UserLocalDataSourceImpl implements UserLocalDataSource {
  Future<Database> database;

  UserLocalDataSourceImpl() {
    initialDatabase();
  }

  Future<Database> initialDatabase() async {
    return openDatabase(
      join(await getDatabasesPath(), 'calcbook_users.db'),
      onCreate: (db, version) {
        return db.execute(
          "CREATE TABLE users(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, phone INTEGER, address TEXT, remnant INTEGER)",
        );
      },
      version: 1,
    );
  }

  @override
  Future<StateActStatus> addUser(String name, String address, int phone) async {
    final db = await initialDatabase();
    final user =
        UserModel(name: name, phone: phone, address: address, remnant: 0);
    if (db != null) {
      await db.insert(
        'users',
        user.toJson(),
        conflictAlgorithm: ConflictAlgorithm.replace,
      );
    }

    return StateActStatus(completed: true, message: 'مشتری با موفقیت اضافه شد');
  }

  @override
  Future<StateActStatus> deleteUser(int id) async {
    final db = await initialDatabase();
    if (db != null) {
      await db.delete(
        'users',
        where: "id = ?",
        whereArgs: [id],
      );
    }

    return StateActStatus(completed: true, message: 'مشتری با موفقیت حذف شد');
  }

  @override
  Future<List<UserModel>> getUsers() async {
    final db = await initialDatabase();
    final List<Map<String, dynamic>> usersList = await db.query('users');
    return List.generate(usersList.length, (i) {
      return UserModel.fromJson(usersList[i]);
    });
  }

  @override
  Future<StateActStatus> updateUser(User user) async {
    final db = await initialDatabase();
    final _u = UserModel(
        id: user.id,
        name: user.name,
        address: user.address,
        phone: user.phone,
        remnant: user.remnant);
    if (db != null) {
      await db.update(
        'users',
        _u.toJson(),
        where: "id = ?",
        whereArgs: [user.id],
      );
    }
    return StateActStatus(
        completed: true, message: 'مشتری با موفقیت ویرایش شد');
  }

  @override
  Future<StateActStatus> callUser(num phone) {
    // TODO: implement callUser
    throw UnimplementedError();
  }
}
