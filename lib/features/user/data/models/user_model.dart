import 'package:iAmCalculatorFlutter/features/user/domain/entities/user.dart';
import 'package:meta/meta.dart';

class UserModel extends User {
  final int id;
  final String name;
  final int phone;
  final String address;
  final int remnant;

  UserModel({this.id, this.name, this.phone, this.address, this.remnant})
      : super(
            id: id,
            name: name,
            phone: phone,
            address: address,
            remnant: remnant);

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'phone': phone,
      'address': address,
      'remnant': remnant,
    };
  }

  factory UserModel.fromJson(Map<String, dynamic> json) {
    return UserModel(
        id: json['id'],
        name: json['name'],
        address: json['address'],
        phone: json['phone'],
        remnant: json['remnant']);
  }
}
