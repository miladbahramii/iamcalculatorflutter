import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:iAmCalculatorFlutter/features/user/domain/entities/user.dart';
import 'package:iAmCalculatorFlutter/features/user/presentation/cubit/user_cubit.dart';
import 'package:iAmCalculatorFlutter/features/user/presentation/widgets/user_form.dart';

import '../../../../injection_container.dart';

class AddOrEditUserPage extends StatelessWidget {
  final User user;

  const AddOrEditUserPage({Key key, this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(user != null ? 'ویرایش مشتری' : 'افزودن مشتری'),
      ),
      // bottomNavigationBar: _bottomNavigationBuilder(),
      body: BlocProvider<UserCubit>(
        create: (_) => sl(),
        child: buildBody(context),
      ),
    );
  }

  Widget buildBody(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => true,
      child: UserForm(user: user,),
    );
  }

  _bottomNavigationBuilder() {}
}
