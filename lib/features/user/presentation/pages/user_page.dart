import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:iAmCalculatorFlutter/core/colors/palette.dart';
import 'package:iAmCalculatorFlutter/core/entities/state_act_status.dart';
import 'package:iAmCalculatorFlutter/features/user/presentation/cubit/user_cubit.dart';
import 'package:iAmCalculatorFlutter/features/user/presentation/pages/add_or_edit_user_page.dart';
import 'package:iAmCalculatorFlutter/features/user/presentation/widgets/users_display.dart';
import 'package:iAmCalculatorFlutter/injection_container.dart';

class UserPage extends StatelessWidget {
  final StateActStatus initialSnackbar;

  const UserPage({Key key, this.initialSnackbar}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text('کاربران'),
        backgroundColor: Palette.primary,
      ),
      body: BlocProvider(
        create: (context) => sl<UserCubit>(),
        child: UsersDisplay(
          initialSnackbar: initialSnackbar,
        ),
      ),
      bottomNavigationBar: _bottomNavigation(context),
    );
  }

  Widget _bottomNavigation(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15),
      child: RaisedButton(
          color: Palette.primary,
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => AddOrEditUserPage()),
            );
          },
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 12),
            child: Text(
              'افزودن مشتری',
              style: TextStyle(color: Colors.white),
            ),
          )),
    );
  }
}
