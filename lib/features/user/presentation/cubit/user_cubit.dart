import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:iAmCalculatorFlutter/core/entities/state_act_status.dart';
import 'package:iAmCalculatorFlutter/core/error/failures.dart';
import 'package:iAmCalculatorFlutter/core/usecases/usecase.dart';
import 'package:iAmCalculatorFlutter/core/utils/input_convertor.dart';
import 'package:iAmCalculatorFlutter/core/widgets/currency_badge.dart';
import 'package:iAmCalculatorFlutter/features/user/domain/entities/user.dart';
import 'package:iAmCalculatorFlutter/features/user/domain/usecases/usecases.dart';
import 'package:meta/meta.dart';

part 'user_state.dart';

const String DATABASE_FAILURE_MESSAGE = 'Database Failure';
const String INVALID_INPUT_FAILURE_MESSAGE =
    'Invalid Input - The number must be a positive integer or zero.';

class UserCubit extends Cubit<UserState> {
  final GetUsers getUsers;
  final AddUser addUser;
  final DeleteUser deleteUser;
  final UpdateUser updateUser;
  final InputConvertor inputConvertor;

  UserCubit(
      {@required this.getUsers,
      @required this.addUser,
      @required this.deleteUser,
      @required this.updateUser,
      @required this.inputConvertor})
      : assert(getUsers != null),
        assert(addUser != null),
        assert(deleteUser != null),
        assert(updateUser != null),
        assert(inputConvertor != null),
        super(Empty());

  void initialUsers({StateActStatus stateActStatus}) async {
    emit(Loading());
    final users = await getUsers(NoParams());
    _usersLoadedOrErrorState(users, stateActStatus);
  }

  void searchInUsers(List<User> users, String searchStr) async {
    emit(Loading());
    if (searchStr.length > 0 && users != null) {
      final List<User> _searchedUsers =
          users.where((element) => element.name.contains(searchStr)).toList();
      emit(UserLoaded(users: _searchedUsers));
    } else {
      initialUsers();
    }
  }

  Future<void> insertUser(String name, String address, String phone) async {
    emit(Loading());
    // final _phone = inputConvertor.stringToUnsignedInteger(phone);
    final phoneInt = getPhoneIntegerOrNull(phone);
    final actionReport =
        await addUser(AddParams(name: name, address: address, phone: phoneInt));
    final StateActStatus stateActStatus =
        await _eitherLoadedOrErrorState(actionReport);
    if (stateActStatus is StateActStatus) {
      initialUsers(stateActStatus: stateActStatus);
    } else {
      throw Error(message: 'unexpected error');
    }
  }

  void removeUser(int id) async {
    final actionReport = await deleteUser(DeleteParams(id: id));
    final StateActStatus stateActStatus =
        await _eitherLoadedOrErrorState(actionReport);

    if (stateActStatus is StateActStatus) {
      initialUsers(stateActStatus: stateActStatus);
    } else {
      throw Error(message: 'unexpected error');
    }
  }

  void editUser(
      User user, String newName, String newAddress, String newPhone) async {
    emit(Loading());
    final actionReport = await updateUser(UpdateParams(
        id: user.id,
        name: newName,
        address: newAddress,
        phone: getPhoneIntegerOrNull(newPhone),
        remnant: user.remnant));
    final StateActStatus stateActStatus =
        await _eitherLoadedOrErrorState(actionReport);

    if (stateActStatus is StateActStatus) {
      initialUsers(stateActStatus: stateActStatus);
    } else {
      throw Error(message: 'unexpected error');
    }
  }

  int getPhoneIntegerOrNull(String phoneStr) {
    final _p = inputConvertor.stringToUnsignedInteger(phoneStr);
    return _p.fold((failure) => 0, (integer) => integer);
  }

  _usersLoadedOrErrorState(Either<Failure, List<User>> failureOrUsers,
      StateActStatus stateActStatus) {
    failureOrUsers.fold(
      (failure) {
        emit(Error(message: _mapFailureToMessage(failure)));
      },
      (users) {
        if (stateActStatus != null) {
          users.length > 0
              ? emit(UserLoaded(
                  users: users,
                  statusMessage: stateActStatus.message,
                  isComplete: stateActStatus.completed))
              : emit(Empty());
        }
        users.length > 0 ? emit(UserLoaded(users: users)) : emit(Empty());
      },
    );
  }

  Future<StateActStatus> _eitherLoadedOrErrorState(
    Either<Failure, StateActStatus> failureOrMessage,
  ) async {
    StateActStatus _s;

    failureOrMessage.fold(
      (failure) {
        _s = StateActStatus(
            completed: false, message: _mapFailureToMessage(failure));
      },
      (payload) {
        _s = StateActStatus(
            completed: payload.completed, message: payload.message);
      },
    );

    return _s;
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case LocalDatabaseFailure:
        return DATABASE_FAILURE_MESSAGE;
      default:
        return 'undefind error';
    }
  }
}
