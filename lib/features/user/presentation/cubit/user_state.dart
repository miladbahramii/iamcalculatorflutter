part of 'user_cubit.dart';

abstract class UserState extends Equatable {
  const UserState();
}

class Empty extends UserState {
  @override
  List<Object> get props => [];
}

class UserLoaded extends UserState {
  final List<User> users;
  final String statusMessage;
  final bool isComplete;

  UserLoaded({@required this.users, this.statusMessage, this.isComplete});

  @override
  List<Object> get props => [users, statusMessage, isComplete];
}

class StatusLoaded extends UserState {
  final String message;
  final bool completed;

  StatusLoaded({@required this.message, @required this.completed});

  @override
  List<Object> get props => [message, completed];
}

class Loading extends UserState {
  @override
  List<Object> get props => [];
}

class Error extends UserState {
  final String message;

  Error({@required this.message});

  @override
  List<Object> get props => [];
}
