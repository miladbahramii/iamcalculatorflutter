import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:iAmCalculatorFlutter/core/colors/palette.dart';
import 'package:iAmCalculatorFlutter/core/entities/state_act_status.dart';
import 'package:iAmCalculatorFlutter/features/user/domain/entities/user.dart';
import 'package:iAmCalculatorFlutter/features/user/presentation/cubit/user_cubit.dart';
import 'package:iAmCalculatorFlutter/features/user/presentation/pages/user_page.dart';

enum Fields { name, phone, address }

class UserForm extends StatefulWidget {
  final User user;
  const UserForm({
    this.user,
    Key key,
  }) : super(key: key);

  @override
  _UserFormState createState() => _UserFormState();
}

class _UserFormState extends State<UserForm> {
  String _name;
  String _phone;
  String _address;
  final _userFormKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();

    _name = widget.user != null ? widget.user.name : '';
    if (widget.user != null) {
      _phone = widget.user.phone != null ? widget.user.phone.toString() : '';
    } else {
      _phone = '';
    }
    _address = widget.user != null ? widget.user.address : '';
  }

  onChangeField(String text, Fields fieldName) {
    switch (fieldName) {
      case Fields.name:
        _name = text;
        break;
      case Fields.phone:
        _phone = text;
        break;
      case Fields.address:
        _address = text;
        break;
      default:
        throw UnimplementedError();
    }
  }

  submitForm(int id) async {
    if (!_userFormKey.currentState.validate()) return 0;

    if (widget.user != null) {
      context.bloc<UserCubit>().editUser(widget.user, _name, _address, _phone);
    } else
      context.bloc<UserCubit>().insertUser(_name, _address, _phone);

    context.bloc<UserCubit>().listen((state) {
      if (state is UserLoaded && state.statusMessage != null) {
        final status = StateActStatus(
            completed: state.isComplete, message: state.statusMessage);
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => UserPage(
                    initialSnackbar: status,
                  )),
        );
      }
    });
  }

  void showSnackbar(String msg) {
    var snackBar = SnackBar(
      content: Text(msg),
    );

    Scaffold.of(context).showSnackBar(snackBar);
  }

  _formValidator(value) {
    if (value.isEmpty) {
      return 'یک نام نظر بگیرید.';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15),
      child: Column(
        children: <Widget>[
          Expanded(
            child: Form(
              key: _userFormKey,
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    TextFormField(
                      initialValue: _name,
                      decoration: const InputDecoration(
                        hintText: '',
                        labelText: 'نام',
                        alignLabelWithHint: true,
                      ),
                      validator: (value) => _formValidator(value),
                      onChanged: (text) => onChangeField(text, Fields.name),
                      // validator: (value) => _validateForm(value),
                    ),
                    TextFormField(
                      initialValue: _phone,
                      decoration: const InputDecoration(
                        hintText: '',
                        labelText: 'تلفن',
                      ),
                      onChanged: (text) => onChangeField(text, Fields.phone),
                    ),
                    TextFormField(
                      initialValue: _address,
                      onChanged: (text) => onChangeField(text, Fields.address),
                      decoration: const InputDecoration(
                        hintText: '',
                        labelText: 'آدرس',
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Container(
            width: double.infinity,
            child: RaisedButton(
              padding: EdgeInsets.symmetric(vertical: 10),
              color: Palette.primary,
              // onPressed: submitForm,
              onPressed: () =>
                  submitForm(widget.user != null ? widget.user.id : null),
              child: Text(
                'ذخیره',
                style: TextStyle(color: Colors.white),
              ),
            ),
          )
        ],
      ),
    );
  }
}
