import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:iAmCalculatorFlutter/core/entities/state_act_status.dart';
import 'package:iAmCalculatorFlutter/features/trade/presentation/widgets/message_display.dart';
import 'package:iAmCalculatorFlutter/features/user/domain/entities/user.dart';
import 'package:iAmCalculatorFlutter/features/user/presentation/cubit/user_cubit.dart';
import 'package:iAmCalculatorFlutter/features/user/presentation/widgets/card_user.dart';
import 'package:iAmCalculatorFlutter/features/user/presentation/widgets/loading_widget.dart';

class UsersDisplay extends StatefulWidget {
  final StateActStatus initialSnackbar;
  const UsersDisplay({
    Key key,
    this.initialSnackbar,
  }) : super(key: key);

  @override
  _UsersDisplayState createState() => _UsersDisplayState();
}

class _UsersDisplayState extends State<UsersDisplay> {
  List<User> usersList;
  DateTime currentBackPressTime;

  @override
  void initState() {
    super.initState();
    context.bloc<UserCubit>().initialUsers();

    if (widget.initialSnackbar != null) {
      showSnackbar(
          widget.initialSnackbar.message, widget.initialSnackbar.completed);
    }

    context.bloc<UserCubit>().listen((state) {
      if (state is UserLoaded && usersList == null) {
        usersList = state.users;
      }
    });
  }

  void searchUsers(String value) {
    context.bloc<UserCubit>().searchInUsers(usersList, value);
  }

  showSnackbar(String message, bool isComplete) async {
    Scaffold.of(context).hideCurrentSnackBar();
    final snackBar = SnackBar(
      backgroundColor: isComplete ? Colors.green : Colors.red,
      content: Text(message,
          style: TextStyle(
            fontSize: 14,
            fontFamily: 'IranSans',
          )),
    );

    Future.delayed(Duration.zero, () {
      Scaffold.of(context).showSnackBar(snackBar);
    });
  }

  Future<bool> onWillPop() {
    DateTime now = DateTime.now();
    if (currentBackPressTime == null ||
        now.difference(currentBackPressTime) > Duration(seconds: 2)) {
      currentBackPressTime = now;
      Fluttertoast.showToast(msg: 'برای خروج دوبار دکمه برگشت را فشار دهید.');
      return Future.value(false);
    }
    exit(0);
  }

  @override
  Widget build(BuildContext _context) {
    return WillPopScope(
      onWillPop: onWillPop,
      child: Container(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: TextFormField(
                  onChanged: (value) => searchUsers(value),
                  decoration: const InputDecoration(
                    hintStyle: TextStyle(fontSize: 16),
                    contentPadding:
                        EdgeInsets.symmetric(vertical: 0, horizontal: 15),
                    hintText: 'جستجو مشتری',
                    alignLabelWithHint: true,
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.blue,
                        width: 1,
                      ),
                      borderRadius: const BorderRadius.all(Radius.circular(50)),
                    ),
                    border: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.grey,
                        width: 1,
                      ),
                      borderRadius: const BorderRadius.all(Radius.circular(50)),
                    ),
                  )),
            ),
            Expanded(
              child: BlocBuilder<UserCubit, UserState>(
                builder: (BuildContext context, state) {
                  if (state is UserLoaded) {
                    if (state.statusMessage != null) {
                      showSnackbar(state.statusMessage, state.isComplete);
                    }

                    return UsersListView(
                      users: state.users,
                    );
                  } else if (state is Loading) {
                    return LoadingWidget();
                  }
                  return MessageDisplay(
                    message: 'هیچ مشتری یافت نشد.',
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class UsersListView extends StatelessWidget {
  final List<User> users;
  const UsersListView({Key key, @required this.users, this.widget})
      : super(key: key);

  final UsersDisplay widget;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: users.length,
      itemBuilder: (_c, index) => CardUser(
        user: users[index],
        inputColor: Colors.white,
      ),
    );
  }
}
