import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:iAmCalculatorFlutter/core/colors/palette.dart';
import 'package:iAmCalculatorFlutter/core/utils/input_convertor.dart';
import 'package:iAmCalculatorFlutter/core/widgets/currency_badge.dart';
import 'package:iAmCalculatorFlutter/core/widgets/headline.dart';
import 'package:iAmCalculatorFlutter/features/trade/presentation/pages/trade_page.dart';
import 'package:iAmCalculatorFlutter/features/user/domain/entities/user.dart';
import 'package:iAmCalculatorFlutter/features/user/presentation/cubit/user_cubit.dart';
import 'package:iAmCalculatorFlutter/features/user/presentation/pages/add_or_edit_user_page.dart';
import 'package:iAmCalculatorFlutter/features/user/presentation/widgets/loading_widget.dart';
import '../../../../injection_container.dart';

enum UserMenuItems {
  edit,
  delete,
  showTransactions,
}

class CardUser extends StatefulWidget {
  // input values
  final User user;
  final Color inputColor;

  static double iconSize = 12;

  CardUser({
    @required this.user,
    this.inputColor,
  });

  @override
  _CardUserState createState() => _CardUserState();
}

class _CardUserState extends State<CardUser> {
  final inputConvertor = InputConvertor();

  Status getStatus(int remnant) {
    if (remnant < 0) {
      return Status.deptor;
    } else if (remnant > 0) {
      return Status.creditor;
    } else {
      return Status.clear;
    }
  }

  void onTapUser() {
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => TradePage(user: widget.user),
        ));
  }

  void onSelectUserMenuItems(UserMenuItems item) {
    switch (item) {
      case UserMenuItems.showTransactions:
        onTapUser();
        break;
      case UserMenuItems.edit:
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => AddOrEditUserPage(user: widget.user),
            ));
        break;
      case UserMenuItems.delete:
        context.bloc<UserCubit>().removeUser(widget.user.id);
        break;
      default:
    }
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: onTapUser,
        child: Padding(
          padding: const EdgeInsets.only(bottom: 1),
          child: Container(
            decoration: BoxDecoration(
                border: Border(
                    bottom: BorderSide(color: Colors.grey.shade400, width: 1))),
            padding: EdgeInsets.fromLTRB(20, 0, 10, 0),
            height: 70,
            child: Row(
              children: <Widget>[
                PopupMenuButton<UserMenuItems>(
                  onSelected: (result) {
                    onSelectUserMenuItems(result);
                  },
                  itemBuilder: (BuildContext context) =>
                      <PopupMenuEntry<UserMenuItems>>[
                    const PopupMenuItem(
                      child: Text('نمایش معاملات'),
                      value: UserMenuItems.showTransactions,
                    ),
                    const PopupMenuItem(
                      child: Text('ویرایش'),
                      value: UserMenuItems.edit,
                    ),
                    const PopupMenuItem(
                      child: Text('حذف'),
                      value: UserMenuItems.delete,
                    ),
                  ],
                  child: Icon(
                    Icons.more_vert,
                    color: Palette.dark,
                  ),
                ),
                Container(
                  child: CircleAvatar(
                    child: Image.asset(
                      'assets/images/user.png',
                      height: 25,
                      width: 25,
                    ),
                  ),
                  height: 50,
                  width: 50,
                ),
                Expanded(
                  flex: 1,
                  child: Headline(
                    color: Palette.dark,
                    text: widget.user.name.toString(),
                  ),
                ),
                Expanded(
                  // flex: 1,
                  child: Align(
                    alignment: Alignment(-1, 0),
                    child: Row(
                      children: [
                        Expanded(
                          child: Text(
                            inputConvertor
                                .numberToCurrency(widget.user.remnant),
                            style: TextStyle(
                                fontFamily: 'IranSansNumber',
                                color: Palette.dark,
                                fontSize: 16,
                                fontWeight: FontWeight.bold),
                            textDirection: TextDirection.ltr,
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        CurrencyBadge(
                          amount: widget.user.remnant,
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
