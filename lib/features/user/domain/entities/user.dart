import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class User extends Equatable {
  final int id;
  final String name;
  final int phone;
  final String address;
  final int remnant;

  User({this.id, @required this.name, this.phone, this.address, this.remnant});

  @override
  List<Object> get props => [id, name, phone, address, remnant];
}
