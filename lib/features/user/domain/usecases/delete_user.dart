import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:iAmCalculatorFlutter/core/entities/state_act_status.dart';
import 'package:iAmCalculatorFlutter/core/error/failures.dart';
import 'package:iAmCalculatorFlutter/core/usecases/usecase.dart';
import 'package:iAmCalculatorFlutter/features/user/domain/repositories/user_repository.dart';

class DeleteUser implements UseCase<StateActStatus, DeleteParams> {
  UserRepository repository;
  DeleteUser(this.repository);

  @override
  Future<Either<Failure, StateActStatus>> call(DeleteParams params) async {
    return await repository.deleteUser(params.id);
  }
}

class DeleteParams extends Equatable {
  final int id;

  DeleteParams({@required this.id});

  @override
  List<Object> get props => [id];
}
