import 'package:dartz/dartz.dart';
import 'package:iAmCalculatorFlutter/core/entities/state_act_status.dart';
import 'package:iAmCalculatorFlutter/core/error/failures.dart';
import 'package:iAmCalculatorFlutter/core/usecases/usecase.dart';
import 'package:iAmCalculatorFlutter/features/user/domain/repositories/user_repository.dart';

class CallUser extends UseCase<StateActStatus, num> {
  final UserRepository repository;

  CallUser(this.repository);

  @override
  Future<Either<Failure, StateActStatus>> call(num userPhone) async {
    return await repository.callUser(userPhone);
  }
}
