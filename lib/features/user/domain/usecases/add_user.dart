import 'package:iAmCalculatorFlutter/core/entities/state_act_status.dart';
import 'package:iAmCalculatorFlutter/core/error/failures.dart';
import 'package:dartz/dartz.dart';
import 'package:iAmCalculatorFlutter/core/usecases/usecase.dart';
import 'package:iAmCalculatorFlutter/features/user/domain/repositories/user_repository.dart';
import 'package:meta/meta.dart';

class AddUser extends UseCase<StateActStatus, AddParams> {
  final UserRepository repository;

  AddUser(this.repository);

  @override
  Future<Either<Failure, StateActStatus>> call(AddParams params) async {
    return await repository.addUser(params.name, params.address, params.phone);
  }
}

class AddParams {
  final String name;
  final String address;
  final int phone;

  AddParams({@required this.name, this.address, this.phone});
}
