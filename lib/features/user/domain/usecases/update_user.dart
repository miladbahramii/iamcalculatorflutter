import 'package:dartz/dartz.dart';
import 'package:iAmCalculatorFlutter/core/entities/state_act_status.dart';
import 'package:iAmCalculatorFlutter/core/error/failures.dart';
import 'package:iAmCalculatorFlutter/core/usecases/usecase.dart';
import 'package:iAmCalculatorFlutter/features/user/domain/repositories/user_repository.dart';
import 'package:meta/meta.dart';

class UpdateUser extends UseCase<StateActStatus, UpdateParams> {
  final UserRepository repository;
  UpdateUser(this.repository);

  @override
  Future<Either<Failure, StateActStatus>> call(UpdateParams params) async {
    return await repository.updateUser(
        params.id, params.name, params.address, params.phone, params.remnant);
  }
}

class UpdateParams {
  final int id;
  final String name;
  final String address;
  final int phone;
  final int remnant;

  UpdateParams(
      {@required this.id,
      @required this.name,
      @required this.address,
      @required this.phone,
      @required this.remnant});
}
