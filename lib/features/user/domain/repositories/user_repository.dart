import 'package:dartz/dartz.dart';
import 'package:iAmCalculatorFlutter/core/entities/state_act_status.dart';
import 'package:iAmCalculatorFlutter/core/error/failures.dart';
import 'package:iAmCalculatorFlutter/features/user/domain/entities/user.dart';

abstract class UserRepository {
  Future<Either<Failure, List<User>>> getUsers();

  Future<Either<Failure, StateActStatus>> addUser(
      String name, String address, int phone);

  Future<Either<Failure, StateActStatus>> deleteUser(int id);

  Future<Either<Failure, StateActStatus>> updateUser(
      int id, String name, String address, int phone, int remnant);

  Future<Either<Failure, StateActStatus>> callUser(num phone);
}
