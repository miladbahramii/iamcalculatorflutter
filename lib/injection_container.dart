import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:get_it/get_it.dart';
import 'package:iAmCalculatorFlutter/core/network/network_info.dart';
import 'package:iAmCalculatorFlutter/core/utils/input_convertor.dart';
import 'package:iAmCalculatorFlutter/core/utils/trade_calculator.dart';
import 'package:iAmCalculatorFlutter/features/trade/data/datasources/trade_local_data_source.dart';
import 'package:iAmCalculatorFlutter/features/trade/data/repositories/trade_repository_impl.dart';
import 'package:iAmCalculatorFlutter/features/trade/domain/repositories/trades_repository.dart';
import 'package:iAmCalculatorFlutter/features/trade/domain/usecases/usecases.dart';
import 'package:iAmCalculatorFlutter/features/trade/presentation/cubit/trade_cubit.dart';
import 'package:iAmCalculatorFlutter/features/user/data/datasources/user_local_data_source.dart';
import 'package:iAmCalculatorFlutter/features/user/data/repositories/user_repository_impl.dart';
import 'package:iAmCalculatorFlutter/features/user/domain/repositories/user_repository.dart';
import 'package:iAmCalculatorFlutter/features/user/domain/usecases/usecases.dart';
import 'package:iAmCalculatorFlutter/features/user/presentation/cubit/user_cubit.dart';

final sl = GetIt.instance;

void init() {
  //! Feature
  initUserFeatureInjection();
  initTradeFeatureInjection();

  //! Core
  sl.registerLazySingleton(() => InputConvertor());
  sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(sl()));
  sl.registerLazySingleton(() => TradeCalculator());
  //! External
  sl.registerLazySingleton(() => DataConnectionChecker());
}

void initUserFeatureInjection() {
  // Cubit
  sl.registerFactory(() => UserCubit(
      addUser: sl(),
      deleteUser: sl(),
      getUsers: sl(),
      inputConvertor: sl(),
      updateUser: sl()));
  // Usecases
  sl.registerLazySingleton(() => AddUser(sl()));
  sl.registerLazySingleton(() => DeleteUser(sl()));
  sl.registerLazySingleton(() => UpdateUser(sl()));
  sl.registerLazySingleton(() => GetUsers(sl()));
  // Repository
  sl.registerLazySingleton<UserRepository>(
      () => UserRepositoryImpl(localDataSource: sl(), networkInfo: sl()));
  // Data Source
  sl.registerLazySingleton<UserLocalDataSource>(
      () => UserLocalDataSourceImpl());
}

void initTradeFeatureInjection() {
  // Cubit
  sl.registerFactory(() => TradeCubit(
        addTrade: sl(),
        deleteTrade: sl(),
        getTrades: sl(),
        updateTrade: sl(),
        updateUser: sl(),
        inputConvertor: sl(),
        tradeCalculator: sl(),
      ));
  // Usecases
  sl.registerLazySingleton(() => AddTrade(sl()));
  sl.registerLazySingleton(() => UpdateTrade(sl()));
  sl.registerLazySingleton(() => DeleteTrade(sl()));
  sl.registerLazySingleton(() => GetTrades(sl()));
  // Repository
  sl.registerLazySingleton<TradeRepository>(
      () => TradeRepositoryImpl(localDataSource: sl(), networkInfo: sl()));
  // Data Source
  sl.registerLazySingleton<TradeLocalDataSource>(
      () => TradeLocalDataSourceImpl());
}
